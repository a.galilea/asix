## CONFIGURACIÓ LOCAL DE XARXA

1. Conceptes
    - Adreça IP, Màscara i Porta d'enllaç
    - Servei Network Manager
    - Paràmetres de xarxa: mtu, vlan, bonding
    - Adreçament dinàmic i estàtic
    - Taula d'enrutament
    - Taula d'adreces mac
    - Resolució de noms
    - Wireshark

2. Comandes
    - ping/traceroute
    - ip
    - ping
    - ifconfig
    - ethtool
    - nmcli (https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/
        html/Networking_Guide/sec-Using_the_NetworkManager_Command_Line_Tool_nmcli.html)
    - arp
    - nslookup
    - nmap
 
3. Configuracions
    - Servei NetworkManager
    - Configuració de xarxa 'live'
    - Configuració de xarxa permanent (ifcfg-...)


ENTREGA

    1. Configureu dos ordinadors fent servir la comanda ip amb dues adreces del rang 172.16.0.0 i comproveu que es 
    veuen entre si. Indiqueu les comandes que heu utilitzat a)per configurar-les b) per comprovar que es veuen.
    2. Com podem descobrir l'adreça mac d'un altre dispositiu de la nostra xarxa (que no sigui el nostre)? Indiqueu-ne 
    les comandes i la sortida.
    3. Com podem canviar el nom d'una interfície de xarxa amb la comanda ip? Poseu exemple
    4. Busqueu el paquet RPM per a la utilitat netperf i instal.leu-lo (copieu netperf al directori 
        cp netperf /usr/local/bin per tal que sigui accessible desde qualsevol lloc). Arrenqueu el servidor en una de 
        les màquines i el client en l'altra tot comprovant la velocitat que obtenim entre els dos. Indiqueu quina es la 
        velocitat obtinguda. Feu servir aquest script en el client per tal de comprovar TCP full-duplex:
         for i in 1
             do
              netperf -H 10.1.2.33 -t TCP_STREAM -B "outbound" -i 10 -P 0 -v 0 \
                -- -s 256K -S 256K &
              netperf -H 10.1.2.33 -t TCP_MAERTS -B "inbound"  -i 10 -P 0 -v 0 \
                -- -s 256K -S 256K &
             done
    5. Canvieu ara la MTU de les interfícies de xarxa dels dos ordinadors a un valor de 9000 bytes. comproveu de nou 
    quina és la velocitat obtinguda. Expliqueu els resultats tot comparant-los.
    6. Realitzeu ara la mateixa configuració estàtica d'adreces IP amb la comanda nmcli. Indiqueu-ne els passos.
    7. On es troba la configuració de servidors de noms de domini?
    8. Realitzeu la mateixa configuració anterior de manera permanent (ifcfg-...). Quins són els paràmetres de la 
    comanda nmcli per tal de forçar que s'apliquin els canvis de xarxa que hem fet als fitxers?
    9. Com podem canviar de manera permanent el nom de les interfícies de xarxa (que sobrevisquin a un reinici?) 
    /etc/udev/rules.d/70-persistent-net.rules
        SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="38:1c:4a:c5:9a:a7", ATTR{type}=="1", 
        KERNEL=="e*", NAME="mi-interfaz"

    10. Com podem traduïr un nom de domini per la seva/seves adreces IP? I com podem trobar el nom de domini a partir 
    d'una adreça IP?
